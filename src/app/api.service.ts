import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  readonly SERVER_ENDPOINT =
    'http://ordenador.westeurope.cloudapp.azure.com:8080';

  constructor(private httpClient: HttpClient) {}

  get(endpoint: string) {
    return this.httpClient.get(`${this.SERVER_ENDPOINT}/${endpoint}`);
  }

  post(endpoint: string, body?: any) {
    return this.httpClient.post(`${this.SERVER_ENDPOINT}/${endpoint}`, body);
  }

  put(endpoint: string, body?: any) {
    return this.httpClient.put(`${this.SERVER_ENDPOINT}/${endpoint}`, body)
  }
}
