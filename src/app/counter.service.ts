import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
@Injectable({
  providedIn: 'root',
})
export class CounterService {
  constructor(private api: ApiService) {}

  getCounter$() {
    return this.api.get('/counter') as Observable<{value: number}>;
  }

  setCounter(value: number) {
    const body = {
      value: value,
    };
    return this.api.post('/counter', body);
  }

  incrementCounter(inc: number) {
    const body = {
      increment: inc,
    };
    return this.api.put('counter', body);
  }
}
