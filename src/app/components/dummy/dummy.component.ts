import { Component, OnInit } from '@angular/core';
import { CounterService } from 'src/app/counter.service';

@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.scss'],
})
export class DummyComponent implements OnInit {
  areButtonsEnabled = false;
  currentValue: number;
  inputValue: number = 0;

  constructor(public counterService: CounterService) {
    this.counterService.getCounter$().subscribe((resp) => {
      this.currentValue = resp.value;
      this.areButtonsEnabled = true;
    });
  }

  ngOnInit(): void {}

  increment() {
    this.areButtonsEnabled = false;
    this.counterService.incrementCounter(this.inputValue).subscribe((_) => {
      this.currentValue += this.inputValue;
      this.areButtonsEnabled = true;
    });
  }

  set() {
    this.areButtonsEnabled = false;
    this.counterService.setCounter(this.inputValue).subscribe((_) => {
      this.currentValue = this.inputValue;
      this.areButtonsEnabled = true;
    });
  }
}
